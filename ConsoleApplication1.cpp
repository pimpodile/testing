#include <iostream>
#include "Array.h"


template<typename T>
void printarr(Array<T>* arr, int index) {
	std::cout << "Platz " << index << ": " << arr->get_value(index) << std::endl;
}

int main()
{
	std::cout << "Hello World!" << std::endl;

	Array<int> arr;
	std::cout << "Array size: " << arr.get_size() << std::endl;
	Array<double> arrd(8);
	std::cout << "Array size: " << arrd.get_size() << std::endl;

	arr.set_value(0, 4);
	printarr(&arr, 0);
	arrd.set_value(1, 1.1111);
	printarr(&arrd, 0);
	printarr(&arrd, 1);

	Array<int> arri;

}