const int initsize = 5;

template <typename T>
class Array {
public:
	
	Array(int size=initsize);

	~Array();

	void set_value(int index, T value);
	T get_value(int index);
	int get_size();
	//void swap(int val1, int val2);

private:
	int size;
	T* arr;
	int id;
	static int count;
};

template<typename T>
int Array<T>::count = 0;

template<typename T>
Array<T>::Array(int size/*=initsize*/) {
	this->arr = new T[size];
	this->size = size;
	this->id = count++;
	for (size_t i = 0; i < this->size; i++) {
		this->arr[i] = 0;
	}
	std::cout << "Constructor called, created " << id << "." << std::endl;
};

template<typename T>
Array<T>::~Array() {
	std::cout << "Destructor " << id << " called." << std::endl;
	delete this->arr;
}

template<typename T>
void Array<T>::set_value(int index, T value) {
	(index < this->size) ? this->arr[index] = value : 1;
}

template<typename T>
T Array<T>::get_value(int index) {
	return this->arr[index];
};

template<typename T>
int Array<T>::get_size() {
	return this->size;
}

